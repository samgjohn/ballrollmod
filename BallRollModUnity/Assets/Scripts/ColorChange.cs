﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChange : MonoBehaviour
{
    public bool clicked;
    
    void Start()
    {

    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if(!clicked)
            {
                GetComponent<Renderer>().material.color = Color.red;
                clicked = true;
            }
            else
            {
                GetComponent<Renderer>().material.color = Color.white;
                clicked = false;
            }
        }
    }
}
