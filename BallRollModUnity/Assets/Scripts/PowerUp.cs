﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script for powerup that allows player to get bigger when picked up.
public class PowerUp : MonoBehaviour
{
    public float multiplier = 10f;

    void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PickUp(other);
        }
    }

    void PickUp(Collider player)
    {
        player.transform.localScale *= multiplier;

        Destroy(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate (new Vector3 (15, 30, 45) * Time.deltaTime);
    }
}
